.. |release| replace:: 3006.1
.. |supported-release-2| replace:: 3005.1
.. |supported-release-3| replace:: 3004.2
.. |supported-release-1-badge| replace:: :bdg-link-success:`3006.1 <https://docs.saltproject.io/en/latest/topics/releases/3006.1.html>`
.. |supported-release-2-badge| replace:: :bdg-link-primary:`3005.1 <https://docs.saltproject.io/en/3005/topics/releases/3005.html>`
.. |supported-release-3-badge| replace:: :bdg-link-dark:`3004.2 <https://docs.saltproject.io/en/3004/topics/releases/3004.2.html>`

.. |release-candidate-version| replace:: 3006.0rc3
.. |debian-release-candidate-gpg| replace:: /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt_rc/salt/py3/debian/11/amd64/latest/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian-release-candidate| replace:: [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg] https://repo.saltproject.io/salt_rc/salt/py3/debian/11/amd64/latest/ bullseye main"
.. |rhel-release-candidate-gpg| replace:: https://repo.saltproject.io/salt_rc/salt/py3/redhat/9/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel-release-candidate| replace:: https://repo.saltproject.io/salt_rc/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel-release-candidate-echo| replace:: 'baseurl=https://repo.saltproject.io/salt_rc/salt/py3/redhat/$releasever/$basearch/latest'
.. |ubuntu-release-candidate-gpg| replace:: /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt_rc/salt/py3/ubuntu/22.04/amd64/latest/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu-release-candidate| replace:: [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg] https://repo.saltproject.io/salt_rc/salt/py3/ubuntu/22.04/amd64/latest/ jammy main"
.. |bootstrap-release-candidate| replace:: python3 git v3006.0rc3
.. |pip-install-release-candidate| replace:: sudo pip install salt==3006.0rc3

.. |amazon-linux2-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-latest-download| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/latest.repo
.. |amazon-linux2-major-gpg| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-major-download| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/3006.repo
.. |amazon-linux2-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |amazon-linux2-minor-download| replace:: https://repo.saltproject.io/salt/py3/amazon/2/x86_64/minor/3006.1.repo

.. |centos9-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |centos9-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3006.repo
.. |centos9-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos9-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3006.1.repo

.. |centos8-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |centos8-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3006.repo
.. |centos8-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos8-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3006.1.repo

.. |centos7-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |centos7-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3006.repo
.. |centos7-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |centos7-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3006.1.repo

.. |debian11-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-latest-download| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/latest bullseye main
.. |debian11-major-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-major-download| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/3006 bullseye main
.. |debian11-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian11-minor-download| replace:: https://repo.saltproject.io/salt/py3/debian/11/amd64/minor/3006.1 bullseye main

.. |debian10-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-latest-download| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/latest buster main
.. |debian10-major-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-major-download| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/3006 buster main
.. |debian10-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |debian10-minor-download| replace:: https://repo.saltproject.io/salt/py3/debian/10/amd64/minor/3006.1 buster main

.. |macos-amd64-download| replace:: https://repo.saltproject.io/salt/py3/macos/latest/salt-3006.1-py3-x86_64.pkg
.. |macos-amd64-gpg| replace:: https://repo.saltproject.io/salt/py3/macos/SALT-PROJECT-GPG-PUBKEY-2023.gpg

.. |photonos4-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/latest.repo
.. |photonos4-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/3006.repo
.. |photonos4-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos4-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/4/x86_64/minor/3006.1.repo

.. |photonos3-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-latest-download| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/latest.repo
.. |photonos3-major-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/latest/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-major-download| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/3006.repo
.. |photonos3-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |photonos3-minor-download| replace:: https://repo.saltproject.io/salt/py3/photon/3/x86_64/minor/3006.1.repo

.. |rhel9-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/latest.repo
.. |rhel9-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/3006.repo
.. |rhel9-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel9-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/9/x86_64/minor/3006.1.repo

.. |rhel8-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/latest.repo
.. |rhel8-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/3006.repo
.. |rhel8-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel8-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/8/x86_64/minor/3006.1.repo

.. |rhel7-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-latest-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/latest.repo
.. |rhel7-major-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-major-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/3006.repo
.. |rhel7-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/SALT-PROJECT-GPG-PUBKEY-2023.pub
.. |rhel7-minor-download| replace:: https://repo.saltproject.io/salt/py3/redhat/7/x86_64/minor/3006.1.repo

.. |ubuntu22-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-latest-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/latest jammy main
.. |ubuntu22-major-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-major-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/3006 jammy main
.. |ubuntu22-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu22-minor-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/22.04/amd64/minor/3006.1 jammy main

.. |ubuntu20-latest-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-latest-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/latest focal main
.. |ubuntu20-major-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-major-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/3006 focal main
.. |ubuntu20-minor-gpg| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |ubuntu20-minor-download| replace:: https://repo.saltproject.io/salt/py3/ubuntu/20.04/amd64/minor/3006.1 focal main

.. |windows-install-exe-example| replace:: Salt-Minion-3006.1-Py3-AMD64-Setup.exe
.. |windows-install-msi-example| replace:: Salt-Minion-3006.1-Py3-AMD64.msi

.. |windows-amd64-exe-gpg| replace:: https://repo.saltproject.io/salt/py3/windows/SALT-PROJECT-GPG-PUBKEY-2023.gpg
.. |windows-amd64-exe-download| replace:: https://repo.saltproject.io/salt/py3/windows/latest/Salt-Minion-3006.1-Py3-AMD64-Setup.exe
