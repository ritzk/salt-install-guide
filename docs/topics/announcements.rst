.. _announcements:

=============
Announcements
=============

.. |date| date::

.. article-info::
    :avatar: ../_static/img/SaltProject_Logomark_teal.png
    :avatar-link: https://saltproject.io/
    :avatar-outline: muted
    :date: Last updated on |date|
    :author: **The Salt Project**
    :class-container: sd-p-2 sd-outline-muted sd-rounded-1


Release announcements
=====================

* **May 5:** `3006.1 <https://docs.saltproject.io/en/latest/topics/releases/3006.1.html>`_ is now available.
* **April 18:** `3006.0 <https://docs.saltproject.io/en/latest/topics/releases/3006.html>`_ is now available.
* **October 4:** `3005.1 <https://docs.saltproject.io/en/latest/topics/releases/3005.1.html>`_ is now available.
* **August 25:** `3005 <https://docs.saltproject.io/en/3005/topics/releases/3005.html>`_ is now available.
* **June 21:** `3004.2 <https://docs.saltproject.io/en/3004/topics/releases/3004.2.html>`_ is now available.
* **June 21:** `3003.5 <https://docs.saltproject.io/en/3003/topics/releases/3003.5.html>`_ is now available.


Security announcements
======================

See `Security announcements <https://saltproject.io/security_announcements/>`_
for the latest announcements.

.. Important::
    To be notified of the latest security updates, subscribe to the
    `Salt security announcements RSS feed <https://saltproject.io/rss-feeds/>`_.
